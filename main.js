const express = require('express')
const app = express()
const PORT = 3000;

//build small REST API with Express

console.log("Server-side program starting...");

app.get('/', (req, res) => {
    res.send('Hello world');
});

app.listen(PORT, () => console.log(
    `Server listening http://localhost:${PORT}`
));